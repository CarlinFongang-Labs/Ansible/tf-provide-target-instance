# Terrarm | Ce manifest permet de Provisionnement une infrastructure AWS [ec2, EBS SG] et de récuperer l'ip de l'instance cible

_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Indications
Ce code terraform permet de provisionner 1 instances ec2 basique sous ubuntu 22.04 LTS (ec2_target_host) et un une instance ec2 sous ubuntu 22.04 LTS avec Ansible préalablement installé (ec2_ansible)

### 0. Créer un bucket pour le stockage du "tfstate" 

Créer un bucket s3 nommé **backend-acd** où sera stocké le fichier **tfstate** qui permettra de décrire à tout instant l'état de l'infrastrucutre.


### 1. Pour lancer l'instance ansible

1. 

````bash
cd tf-provide-target-instance/ec2_ansible
````
Ajuter la valeur des variables **`instance_type, size_ebs`** dans le fichier **main.tf** au besoin 

2. 

````bash
export AWS_ACCESS_KEY_ID='AKIA47CRYS47BS5F2ZXU'
export AWS_SECRET_ACCESS_KEY='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'
export AWS_SESSION_TOKEN='550e8400-e29b-41d4-a716-446655440079'
````

3. 
Copier la clé ssh générer à partir d'AWS dans le repertoire `tf-provide-target-instance/secret/`

4. 

````bash
cd tf-provide-target-instance/ec2_ansible/
terraform init -reconfigure #permet d'écraser les précédentes configurations terraform dans ce repertoire
terraform plan
terraform apply
````

Enjoy, l'instance ansible est prête à l'emploi 🥳

### 2. provisionner l'instances qui servira de machine cible paur les opérations de test à réaliser réaliser

Se rendre dans le repertoire `tf-provide-target-instance/ec2_target_host` et reprendre les étapes énumérées précédement

